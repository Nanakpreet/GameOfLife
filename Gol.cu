#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cuda_runtime.h>



__device__ 
int countNeighbors(int col, int row, int row_max, int col_max, int *world)
{
	int myCount = 0;
	for (int i=-1;i<2;i++)
		for(int j=-1;j<2;j++)
		{
			int my_row, my_col;
			my_col = col+i;
			my_row = row+j;
			if (my_col==-1)  my_col = col_max-1;
			if (my_row==-1)  my_row = row_max-1;
			if (my_col==col_max) my_col=0;
			if (my_row==row_max) my_row=0;



			if (i==0 && j==0) 
				continue;
			else
			{
				myCount+=world[(my_col)*row_max + my_row];
			}
		}
	
	return myCount;
}



// kernel definition

__global__ void countAllNeighbors(int row_max, int col_max, int *world, int *otherWorld)
{
	
	int idx = blockIdx.x * blockDim.x + threadIdx.x;

	if (idx >= row_max * col_max)
		return;

	int row = idx / row_max;
	int col = idx % row_max; 
	
	otherWorld[idx]=countNeighbors(row,col,row_max,col_max, world);
	
}

//kernel definition

__global__ void updateWorld(int row_max, int col_max, int *world, int *otherWorld)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;

	if (idx >= row_max * col_max)
		return;

			if (otherWorld[idx]==3) world[idx]=1;
			else if (otherWorld[idx]!=2) world[idx]=0;
}

int main()
{
	const int row_max = 20;
	const int col_max = 30;
	int *A, *B;
	int N = ((row_max*col_max)/1024)+1;
	cudaMallocManaged(&A, row_max*col_max*sizeof(int));
	cudaMallocManaged(&B, row_max*col_max*sizeof(int));

	for (int i=0; i<row_max*col_max; i++)
	{
		A[i]=0;
		B[i]=0;
	}

	//Glider
	A[1*row_max+0] = 1;
	A[1*row_max+2] = 1;
	A[2]=1;
	A[2*row_max+1]=1;
	A[2*row_max+2]=1;

	

       

	//while (true)
	for (int i = 0; i<1000; i++)
	{
		for (int c=0;c<col_max;c++)
		{
			for (int r=0;r<row_max;r++)
				printf("%d",A[c*row_max+r]);
			printf("\n");
		}
		fflush(stdout);

		countAllNeighbors<<<N,1024>>>(row_max, col_max, A, B);
		updateWorld<<<N,1024>>>(row_max, col_max, A, B);

		cudaDeviceSynchronize();
		
		
		
		usleep(100000);
		printf("\n");
	
	}
	cudaFree(A);
	cudaFree(B);
}